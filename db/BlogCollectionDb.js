const db = require("./MongoConnect").db("Blogging");

const blogCollection = db.collection("blogs");

// To save a blog
const saveBlog = async (blog) => await blogCollection.insertOne(blog);

const isOwnerOfTheBlog = async (uid, bid) => {
  const result = await blogCollection.findOne({ uid, bid });
  return result !== null;
};

const isTeamOfTheBlog = async (team, bid) => {
  const result = await blogCollection.findOne({ bid, team });
  return result !== null;
};

// Get the entire blog details by bid
const getBlogById = async (bid) => await blogCollection.findOne({ bid });

const getAllBlogDynamicly = async (findBy, projectBy) =>
  await blogCollection.find(findBy).project(projectBy).toArray();

// Get all the blog by uid
const getAllBlogByUid = async (uid) =>
  await blogCollection
    .find({ uid })
    .project({
      _id: 0,
      bid: 1,
      title: 1,
      content: 1,
      uid: 1,
      team: 1,
      image: 1,
    })
    .toArray();

const getAllBlogByTeamUid = async (current, team) =>
  await blogCollection
    .find({ team, uid: current })
    .project({ _id: 0, bid: 1, title: 1, content: 1, uid: 1, image: 1 })
    .toArray();

// Get all blogs with the user fields from user Collection
const getAllBlogs = async () =>
  await blogCollection
    .aggregate([
      {
        $lookup: {
          from: "users",
          localField: "uid",
          foreignField: "uid",
          as: "UserDetails",
        },
      },
      {
        $project: {
          _id: 0,
          uid: 1,
          bid: 1,
          title: 1,
          content: 1,
          UserDetails: {
            name: 1,
          },
        },
      },
      {
        $unwind: "$UserDetails",
      },
    ])
    .toArray();

// Get blog by bid
const findBlogByBid = async (bid) => {
  const result = await blogCollection.findOne(
    { bid: bid },
    {
      projection: {
        _id: 0,
        uid: 1,
        team: 1,
      },
    }
  );

  console.log(result);
  return result;
};

// Add team members in blog
const addTeamInBlog = async (uid, blogList) => {
  await blogCollection.updateMany(
    { bid: { $in: blogList } },
    { $addToSet: { team: uid } }
  );
};

const removeTeamFromBlogById = async (tid, bid) =>
  await blogCollection.updateOne({ bid }, { $pull: { team: tid } });

// const getBlogById = async (bid) =>
//   await blogCollection
//     .aggregate([
//       {
//         $lookup: {
//           from: "users",
//           localField: "uid",
//           foreignField: "uid",
//           as: "UserDetails",
//         },
//       },
//       {
//         $project: {
//           _id: 0,
//           bid: 1,
//           title: 1,
//           content: 1,
//           image: 1,
//           editedOn: 1,
//           createdOn: 1,
//           UserDetails: {
//             name: 1,
//           },
//         },
//       },
//       {
//         $unwind: "$UserDetails",
//       },
//     ])
//     .toArray();

// const checkOwnerShipOfBlog = async (uid, bid) =>
//   await blogCollection.findOne({ uid, bid });

// Delete blog by bid
const deleteParticularBlog = async (bid) =>
  await blogCollection.deleteOne({ bid });

//Check if the same title of blog exist for the uid
const checkDuplicateTitle = async (title, uid) => {
  const result = await blogCollection.findOne({
    title: { $regex: new RegExp(title, "i") },
    uid,
  });
  return result === null;
};

//Check if the same title of blog exist for the uid
const checkDuplicateTitleForUpdate = async (title, uid, bid) => {
  const result = await blogCollection.findOne({
    title: { $regex: new RegExp(title, "i") },
    uid,
    bid: { $ne: bid },
  });
  return result === null;
};

// Update blog by bid and the blog object passed
const updateParticularBlog = async (bid, blog) => {
  // Fetching the original blog detials
  const previousBlog = await blogCollection.findOne(
    { bid },
    { projection: { uid: 1, title: 1, content: 1, image: 1 } }
  );

  console.log(blog);
  // Updating the changes in the previous blog
  for (let [key, value] of Object.entries(blog)) {
    previousBlog[key] = value;
  }

  // Updating the blog Collection
  const result = await blogCollection.updateOne(
    { bid },
    { $set: { ...previousBlog } }
  );

  console.log(result);
  return result;
};

//Remove a particular member from every blog of a company

const removeMemberFromEveryBlog = async (uid, tid) =>
  await blogCollection.updateMany({ uid, team: tid }, { $pull: { team: tid } });

//Delete all blog for deleted master
const deleteAllBlogsByUid = async (uid) =>
  await blogCollection.deleteMany({ uid });

const deleteTeamFromAllBlog = async (uid) =>
  blogCollection.updateMany({ team: uid }, { $pull: { team: uid } });

const dropallpost = async () => await blogCollection.drop();

//
const getBlogWithTeam = async (uid, matchBy, project) =>
  await blogCollection
    .aggregate([
      { $match: matchBy },
      {
        $lookup: {
          from: "users",
          localField: "team",
          foreignField: "uid",
          as: "TeamDetails",
        },
      },
      {
        $project: project,
      },
    ])
    .toArray();

module.exports = {
  saveBlog,
  getBlogById,
  isOwnerOfTheBlog,
  isTeamOfTheBlog,
  getAllBlogDynamicly,
  getAllBlogByUid,
  getAllBlogByTeamUid,
  getAllBlogs,
  getBlogWithTeam,
  findBlogByBid,
  addTeamInBlog,
  removeTeamFromBlogById,
  deleteParticularBlog,
  checkDuplicateTitle,
  checkDuplicateTitleForUpdate,
  updateParticularBlog,
  removeMemberFromEveryBlog,
  deleteAllBlogsByUid,
  deleteTeamFromAllBlog,
  dropallpost,
};
