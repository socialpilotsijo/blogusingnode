const db = require("./MongoConnect").db("Blogging");

const userCollection = db.collection("users");

const registerUser = async (user) => await userCollection.insertOne(user);

//Get a particular user by email
const getUserByEmail = async (email) =>
  await userCollection.findOne(
    { email },
    {
      projection: {
        _id: 0,
        uid: 1,
        email: 1,
        rights: 1,
        password: 1,
        master: 1,
        current: 1,
      },
    }
  );

const getTeamMembers = async (mid) =>
  await userCollection
    .find({ master: mid })
    .project({ _id: 0, uid: 1, name: 1, email: 1 })
    .toArray();

const getUserById = async (uid) => await userCollection.findOne({ uid });

// const getTeamMembersWithAssignedBlogs = async (mid) =>
//   await userCollection.aggregate([
//     {
//       $match: { uid: mid },
//     },
//     {
//       $lookup: {
//         from: users,
//         localField: uid,
//         foreignField: master,
//         as: Team,
//       },
//     },
//   ]);

//Check if the current master is valid
const checkIfCurrentIsValid = async (uid, mid) => {
  const result = await userCollection.findOne({ uid, current: mid });
  return result !== null;
};

//Check if the uid has this master
const checkIfMasterIsValid = async (uid, mid) => {
  const result = await userCollection.findOne({ uid, master: mid });
  return result !== null;
};

//Check if the user exist by uid
const isUserExistById = async (uid) => {
  const result = await userCollection.find({ uid });
  return result !== null;
};

// Check if the particular email exist in user Collection
const isEmailExist = async (email) => {
  const result = await userCollection.findOne({ email });
  return result !== null;
};

// delete User by uid
const deleteUserByUid = async (uid) => await userCollection.deleteOne({ uid });

// Delete Master from all
const deleteMasterFromAllUser = async (uid) =>
  await userCollection.updateMany({ master: uid }, { $pull: { master: uid } });

//Set current to null for uid

const setCurrentToNull = async (uid) =>
  await userCollection.updateOne({ uid }, { $set: { current: null } });

// Set Current to null whose master is uid
const setCurrentToNullForMid = async (uid) =>
  await userCollection.updateOne({ current: uid }, { $set: { current: null } });

// Get the rights of the user
const getRights = async (uid) =>
  await userCollection.findOne(
    { uid },
    {
      projection: {
        _id: 0,
        rights: 1,
      },
    }
  );

// Get the user details for invitation
const getInviterDetails = async (uid) =>
  await userCollection.findOne(
    { uid },
    { projection: { _id: 0, company: 1, name: 1, email: 1, logo: 1 } }
  );

// Adding master to the master list
const addMaster = async (uid, mid) => {
  const result = await userCollection.updateOne(
    { uid },
    { $addToSet: { master: mid } }
  );
  console.log(result);
};

//Remove master from the user table
const removeMaster = async (uid, mid) =>
  await userCollection.updateOne({ uid }, { $pull: { master: mid } });

// Get Master by uid
const getMaster = async (uid) =>
  await userCollection.findOne({ uid }, { projection: { _id: 0, master: 1 } });

// Set current(master) of the user
const setCurrentMaster = async (uid, mid) =>
  await userCollection.updateOne({ uid }, { $set: { current: mid } });

// Get the master list and the current master of user
const getMasterList = async (uid) =>
  await userCollection.findOne(
    { uid },
    { projection: { _id: 0, current: 1, master: 1 } }
  );

const getAllMasterListWithName = async (uid) =>
  await userCollection
    .aggregate([
      {
        $match: {
          uid,
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "master",
          foreignField: "uid",
          as: "MasterList",
        },
      },
      {
        $project: {
          uid: 1,
          MasterList: {
            uid: 1,
            name: 1,
            company: 1,
          },
        },
      },
    ])
    .toArray();

const getTeamWithBlogs = async (uid, matchBy, projectBy) =>
  await userCollection
    .aggregate([
      { $match: matchBy },
      {
        $lookup: {
          from: "blogs",
          localField: "uid",
          foreignField: "team",
          as: "BlogDetails",
        },
      },
      {
        $project: projectBy,
      },
    ])
    .toArray();

// Get master with the master name of particular user (by uid)
const getMasterListWithName = async (uid) =>
  await userCollection
    .aggregate([
      {
        $match: {
          uid,
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "current",
          foreignField: "uid",
          as: "CurrentMasterDetails",
        },
      },
      {
        $project: {
          _id: 0,
          uid: 1,
          name: 1,
          current: 1,
          master: 1,
          CurrentMasterDetails: {
            name: 1,
            company: 1,
          },
        },
      },
    ])
    .toArray();

const dropallusers = async () => await userCollection.drop();

module.exports = {
  registerUser,
  getUserByEmail,
  getUserById,
  getTeamWithBlogs,
  getTeamMembers,
  checkIfCurrentIsValid,
  checkIfMasterIsValid,
  isUserExistById,
  isEmailExist,
  deleteUserByUid,
  deleteMasterFromAllUser,
  setCurrentToNull,
  setCurrentToNullForMid,
  getRights,
  getInviterDetails,
  addMaster,
  removeMaster,
  getMaster,
  getAllMasterListWithName,
  setCurrentMaster,
  getMasterList,
  getMasterListWithName,
  dropallusers,
};
